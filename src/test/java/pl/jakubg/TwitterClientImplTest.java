package pl.jakubg;

import pl.jakubg.exceptions.WorkdayAppException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.jakubg.git.GitProjectItemTO;
import pl.jakubg.oauth.OAuthTO;
import pl.jakubg.twitter.TweetTO;
import pl.jakubg.twitter.TwitterClientImpl;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TwitterClientImplTest {

    private static final String TWITTER_SEARCH_URL = "https://api.twitter.com/1.1/search/tweets.json";

    @Mock
    private RestTemplate restTemplate;

    private TwitterClientImpl sut;

    @Before
    public void init() {
        OAuthTO oAuthTO = new OAuthTO();

        sut = new TwitterClientImpl(restTemplate, oAuthTO);
    }

    @Test
    public void findTweetsTest() throws Exception {

        String url = TWITTER_SEARCH_URL + "?q=Cocacola01&count=10";

        when(restTemplate.exchange(
                ArgumentMatchers.eq(url),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())).thenReturn(successfulResponse());

        List<TweetTO> results = sut.findTweets("Cocacola01");

        assertNotNull(results);
        assertEquals(5, results.size());

    }

    @Test(expected = WorkdayAppException.class)
    public void unsuccessfulRequestFindTweets() throws Exception {

        String url = TWITTER_SEARCH_URL + "?q=Cocacola01&count=10";

        when(restTemplate.exchange(
                ArgumentMatchers.eq(url),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())).thenReturn(unsuccessfulResponse());

        List<TweetTO> results = sut.findTweets("Cocacola01");
    }

    @Test
    public void retrieveTweetsForGitProjectTest() throws Exception {

        List<GitProjectItemTO> result = getListOfGitProjects();

        when(restTemplate.exchange(
                ArgumentMatchers.any(String.class),
                ArgumentMatchers.any(HttpMethod.class),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<String>>any())).thenReturn(successfulResponse());

        sut.retrieveTweetsForGitProject(result);

        assertNotNull(result);
        assertEquals(2, result.size());

        GitProjectItemTO gitProjectItemTO1 = result.get(0);
        assertNotNull(gitProjectItemTO1.getTweetTOList());
        assertEquals(5, gitProjectItemTO1.getTweetTOList().size());

        GitProjectItemTO gitProjectItemTO2 = result.get(1);
        assertNotNull(gitProjectItemTO2.getTweetTOList());
        assertEquals(5, gitProjectItemTO2.getTweetTOList().size());
    }

    private List<GitProjectItemTO> getListOfGitProjects() {

        List<GitProjectItemTO> result = new ArrayList<>();

        GitProjectItemTO itemTO1 = new GitProjectItemTO();
        itemTO1.setId(1);
        itemTO1.setName("Cocacola1");
        itemTO1.setFullName("Full/CocacolaName1");
        itemTO1.setHtmlUrl("https://github.com/cocacola1");
        itemTO1.setCloneUrl("https://github.com/CocacolaName1/CocacolaName1.git");

        result.add(itemTO1);

        GitProjectItemTO itemTO2 = new GitProjectItemTO();
        itemTO2.setId(2);
        itemTO2.setName("Cocacola2");
        itemTO2.setFullName("Full/CocacolaName2");
        itemTO2.setHtmlUrl("https://github.com/cocacola2");
        itemTO2.setCloneUrl("https://github.com/CocacolaName2/CocacolaName2.git");

        result.add(itemTO2);

        return result;
    }

    private ResponseEntity<String> successfulResponse() throws Exception {

        String data = loadResponseData();

        ResponseEntity responseEntity = new ResponseEntity(data, HttpStatus.OK);

        return responseEntity;
    }

    private ResponseEntity<String> unsuccessfulResponse() throws Exception {
        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
        return responseEntity;
    }

    private String loadResponseData() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        URI resourceLocation = classLoader.getResource("twitter_response.txt").toURI();

        String content = new String(Files.readAllBytes(Paths.get(resourceLocation)), "UTF-8");

        return content;
    }
}

package pl.jakubg;

import pl.jakubg.exceptions.WorkdayAppException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.jakubg.oauth.OAuthClientImpl;
import pl.jakubg.oauth.OAuthTO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OAuthClientImplTest {

    private OAuthClientImpl sut;
    private String oAuthConsumerKey = "dummy_value1";
    private String oAuthConsumerSecret = "dummy_value2";

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void init() {

        sut = new OAuthClientImpl(restTemplate, oAuthConsumerKey, oAuthConsumerSecret);
    }

    @Test
    public void getOAuthTest() throws WorkdayAppException {

        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(prepareSuccessfulResponse());

        OAuthTO oAuthTO = sut.getOAuth();

        assertNotNull(oAuthTO);
        assertEquals("BBAAAAAAAAAAAAAAAAAAAO6b9QAAAAAAj%2BHzW%2FXaYDlBs0SRDRq9IgHPHgQ%3DDmRWcZfjROpRVpfMTClQggmtXXLG9hg4Zcn5yCwG9wZafQ2ic3", oAuthTO.getoAuthToken());
    }

    @Test(expected = WorkdayAppException.class)
    public void getOAuthFailedTest() throws Exception {
        when(restTemplate.postForEntity(anyString(), any(HttpEntity.class), any(Class.class))).thenReturn(prepareUnsuccessfulResponse());

        OAuthTO oAuthTO = sut.getOAuth();
    }

    private String oAutTokenJSONResponse() {
        String resp = "{\"token_type\":\"bearer\",\"access_token\":\"BBAAAAAAAAAAAAAAAAAAAO6b9QAAAAAAj%2BHzW%2FXaYDlBs0SRDRq9IgHPHgQ%3DDmRWcZfjROpRVpfMTClQggmtXXLG9hg4Zcn5yCwG9wZafQ2ic3\"}";
        return resp;
    }

    private ResponseEntity prepareSuccessfulResponse() {
        String responseBody = oAutTokenJSONResponse();
        return new ResponseEntity(responseBody, HttpStatus.OK);
    }

    private ResponseEntity prepareUnsuccessfulResponse() {
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }
}

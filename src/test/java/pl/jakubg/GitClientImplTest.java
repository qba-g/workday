package pl.jakubg;

import pl.jakubg.exceptions.WorkdayAppException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.jakubg.git.GitClientImpl;
import pl.jakubg.git.GitProjectItemTO;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GitClientImplTest {

    @Mock
    private RestTemplate restTemplate;

    private GitClientImpl sut;
    private final static String URL = "https://api.github.com/search/repositories?q=Cocacola";

    @Before
    public void init() {
        sut = new GitClientImpl(restTemplate);
    }

    @Test
    public void successfulRequestTest() throws Exception {

        when(restTemplate.getForEntity(URL, String.class)).thenReturn(prepareSuccessfulResponse());

        List<GitProjectItemTO> responseTOList = sut.projectGetRequest("Cocacola");

        assertNotNull(responseTOList);
        assertEquals(30, responseTOList.size());
    }

    @Test(expected = WorkdayAppException.class)
    public void unsuccessfulRequestTest() throws Exception {

        when(restTemplate.getForEntity(URL, String.class)).thenReturn(prepareUnsuccessfulResponse());

        sut.projectGetRequest("Cocacola");
    }

    private ResponseEntity<String> prepareSuccessfulResponse() throws Exception {

        String data = loadResponseData();

        ResponseEntity responseEntity = new ResponseEntity(data, HttpStatus.OK);

        return responseEntity;
    }

    private ResponseEntity<String> prepareUnsuccessfulResponse() throws Exception {

        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);

        return responseEntity;
    }

    private String loadResponseData() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        URI resourceLocation = classLoader.getResource("git_response.txt").toURI();

        String content = new String(Files.readAllBytes(Paths.get(resourceLocation)), "UTF-8");

        return content;
    }
}

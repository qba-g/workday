/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.git;

import pl.jakubg.twitter.TweetTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple transfer object used to extract most interesting data related to Git project.
 */
public class GitProjectItemTO {

    private int id;
    private String name;
    private String fullName;
    private String cloneUrl;
    private String htmlUrl;

    private List<TweetTO> tweetTOList = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getCloneUrl() {
        return cloneUrl;
    }

    public void setCloneUrl(String cloneUrl) {
        this.cloneUrl = cloneUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public List<TweetTO> getTweetTOList() {
        return tweetTOList;
    }

    public void setTweetTOList(List<TweetTO> list) {
        tweetTOList = list;
    }

    @Override
    public String toString() {
        return "GitProjectItemTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", fullName='" + fullName + '\'' +
                ", cloneUrl='" + cloneUrl + '\'' +
                ", htmlUrl='" + htmlUrl + '\'' +
                ", tweetTOList=" + tweetTOList +
                '}';
    }
}

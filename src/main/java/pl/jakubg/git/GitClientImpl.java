/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.git;

import pl.jakubg.exceptions.WorkdayAppException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by kuba on 2019-01-30.
 */
public class GitClientImpl implements GitClient {

    private static final Logger LOGGER = LogManager.getLogger(GitClientImpl.class);

    private final static String GITHUB_SEARCH_URL = "https://api.github.com/search/repositories?q=";

    private RestTemplate restTemplate;

    public GitClientImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<GitProjectItemTO> projectGetRequest(String projectName) throws WorkdayAppException {

        String resourceUrl = getProjectQuery(projectName);

        ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            LOGGER.info("Successfully received response from REST endpoint.");
            return JsonParser.convertJSONBody(response);

        } else {
            LOGGER.error("Unsuccessful request to REST endpoint: " + response.getStatusCode());
            throw new WorkdayAppException("Incorrect result when calling GET for GIT resource");
        }
    }

    private String getProjectQuery(String projectName) {

        String resourceURL = GITHUB_SEARCH_URL + projectName;
        LOGGER.info("Preparing request to: " + resourceURL);
        return resourceURL;
    }

}

/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.git;

import pl.jakubg.exceptions.WorkdayAppException;

import java.util.List;

/**
 * Define methods that perform operations on GIT repository.
 */
public interface GitClient {

    public List<GitProjectItemTO> projectGetRequest(String projectName) throws WorkdayAppException;
}

/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.git;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriUtils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains all types of parsers that work with JSON responses related to GIT Projects.
 */
public class JsonParser {

    public static List<GitProjectItemTO> convertJSONBody(ResponseEntity<String> response) {

        String jsonBody = response.getBody().toString();
        JSONObject jsonObject = new JSONObject(jsonBody);
        JSONArray jsonArray = jsonObject.getJSONArray("items");

        List<GitProjectItemTO> gitProjectItemTOList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject item = jsonArray.getJSONObject(i);
            GitProjectItemTO gitProjectItemTO = convertJSONItem(item);
            gitProjectItemTOList.add(gitProjectItemTO);
        }

        return gitProjectItemTOList;
    }

    public static GitProjectItemTO convertJSONItem(JSONObject item) {

        GitProjectItemTO gitProjectItemTO = new GitProjectItemTO();

        gitProjectItemTO.setId(item.getInt("id"));
        gitProjectItemTO.setName(item.getString("name"));
        gitProjectItemTO.setFullName(item.getString("full_name"));
        gitProjectItemTO.setCloneUrl(item.getString("clone_url"));

        return gitProjectItemTO;
    }

    public static String convert(GitProjectItemTO itemTO) {
        JSONObject jsonObject = new JSONObject(itemTO);
        System.out.println(jsonObject.toString());

        return jsonObject.toString();
    }

    public static String convert(List<GitProjectItemTO> itemTOList) {
        JSONArray jsonArray = new JSONArray(itemTOList);
        return jsonArray.toString(2);
    }
}

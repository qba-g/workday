/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.exceptions;

/**
 * Generic application exception representing all kinds of problems.
 */
public class WorkdayAppException extends Exception {

    public WorkdayAppException() {
        super();
    }

    public WorkdayAppException(String message) {
        super(message);
    }
}

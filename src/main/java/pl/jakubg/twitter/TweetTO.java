/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.twitter;

/**
 * Provide simplified view of tweeter entries.
 */
public class TweetTO {

    //"created_at"
    private String createdAt;
    //text
    private String text;

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TweetTO{" +
                "createdAt='" + createdAt + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}

/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.twitter;

import pl.jakubg.exceptions.WorkdayAppException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriUtils;
import pl.jakubg.App;
import pl.jakubg.git.GitProjectItemTO;
import pl.jakubg.oauth.OAuthTO;

import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Implementation of methods that retrive data from Twitter REST services.
 */
public class TwitterClientImpl implements TwitterClient {

    private static final Logger LOGGER = LogManager.getLogger(TwitterClientImpl.class);

    private static final String TWITTER_SEARCH_URL = "https://api.twitter.com/1.1/search/tweets.json";

    private RestTemplate restTemplate;

    private OAuthTO oAuthTO;

    public TwitterClientImpl(RestTemplate restTemplate, OAuthTO oAuthTO) {
        this.restTemplate = restTemplate;
        this.oAuthTO = oAuthTO;
    }

    private String getTwitterSearchUrl(String topic) {

        String topicUriEncoded = UriUtils.encodeQuery(topic, StandardCharsets.UTF_8.toString());
        String searchURL = TWITTER_SEARCH_URL + "?q=" + topicUriEncoded + "&count=10";

        return searchURL;
    }

    public List<TweetTO> findTweets(String topic) throws WorkdayAppException {

        String url = getTwitterSearchUrl(topic);
        LOGGER.info("Preparing request to URL: " + url);

        HttpHeaders httpHeaders = prepareRequestHeaders(oAuthTO);
        HttpEntity entity = new HttpEntity(httpHeaders);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

        if (HttpStatus.OK.equals(response.getStatusCode())) {
            LOGGER.info("Successful request");
            List<TweetTO> list = JsonParser.convertJSONBody(response);
            return list;
        } else {
            LOGGER.error("Unsuccessful request, status code: " + response.getStatusCode());
            throw new WorkdayAppException("Problem with issuing twitter query to the server.");
        }
    }

    private HttpHeaders prepareRequestHeaders(OAuthTO oAuthTO) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", App.APP_NAME);
        headers.set("Authorization", "Bearer " + oAuthTO.getoAuthToken());

        return headers;
    }

    public void retrieveTweetsForGitProject(List<GitProjectItemTO> result) throws WorkdayAppException {

        for (GitProjectItemTO gitProjectItemTO : result) {

            String tweetTopicToFind = gitProjectItemTO.getName();
            List<TweetTO> tweetTOList = findTweets(tweetTopicToFind);
            gitProjectItemTO.setTweetTOList(tweetTOList);
        }
    }

}

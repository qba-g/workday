/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.twitter;

import pl.jakubg.exceptions.WorkdayAppException;
import pl.jakubg.git.GitProjectItemTO;

import java.util.List;

/**
 * Define methods that operates on twitter REST services.
 */
public interface TwitterClient {

    public void retrieveTweetsForGitProject(List<GitProjectItemTO> result) throws WorkdayAppException;

    public List<TweetTO> findTweets(String topic) throws WorkdayAppException;
}

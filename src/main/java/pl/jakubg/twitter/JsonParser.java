/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.twitter;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Provide methods that operate on Twitter JSON responses.
 */
public class JsonParser {

    public static List<TweetTO> convertJSONBody(ResponseEntity<String> response) {

        String jsonBody = response.getBody().toString();

        JSONObject jsonObject = new JSONObject(jsonBody);
        JSONArray jsonArray = jsonObject.getJSONArray("statuses");

        List<TweetTO> tweetItemList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject item = jsonArray.getJSONObject(i);
            TweetTO tweetTO = convertJSONItem(item);
            tweetItemList.add(tweetTO);
        }

        return tweetItemList;
    }

    public static TweetTO convertJSONItem(JSONObject item) {

        TweetTO tweetTO = new TweetTO();

        tweetTO.setCreatedAt(item.getString("created_at"));
        tweetTO.setText(item.getString("text"));

        return tweetTO;
    }
}

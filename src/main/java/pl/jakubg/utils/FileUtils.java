/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.utils;

import pl.jakubg.exceptions.WorkdayAppException;
import pl.jakubg.git.GitProjectItemTO;
import pl.jakubg.git.JsonParser;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Utility class that provide File related operations.
 */
public class FileUtils {

    public static void saveResultsToFile(String fileName, String defaultOutputFileName, List<GitProjectItemTO> gitResult) throws WorkdayAppException {

        if (fileName == null || fileName.isEmpty()) {
            fileName = defaultOutputFileName;
        }

        Path path = Paths.get(fileName);

        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            String jsonResults = JsonParser.convert(gitResult);
            writer.write(jsonResults);
        } catch (IOException ioe) {
            throw new WorkdayAppException("Can't write results to file: " + fileName);
        }
    }
}

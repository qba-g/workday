/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.properties;

import java.util.ResourceBundle;

/**
 * Perform operations on application configuration properties.
 */
public class LoadProperties {

    private static final String PROPERTY_FILE_NAME = "application";
    private ResourceBundle rb;

    public LoadProperties() {
        rb = ResourceBundle.getBundle(PROPERTY_FILE_NAME);
    }

    public String getProperty(String name) {
        return rb.getString(name);
    }
}

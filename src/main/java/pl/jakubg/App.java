/**
 * @author: Jakub Gzyl
 */

package pl.jakubg;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.RestTemplate;
import pl.jakubg.commandline.Cmd;
import pl.jakubg.commandline.CmdConfigOption;
import pl.jakubg.git.GitClient;
import pl.jakubg.git.GitClientImpl;
import pl.jakubg.git.GitProjectItemTO;
import pl.jakubg.oauth.OAuthClient;
import pl.jakubg.oauth.OAuthClientImpl;
import pl.jakubg.properties.LoadProperties;
import pl.jakubg.twitter.TwitterClientImpl;
import pl.jakubg.utils.FileUtils;

import java.util.List;

/**
 * Workday recruitment application.
 */
public class App {

    private static final Logger LOGGER = LogManager.getLogger(App.class);

    public static final String APP_NAME = "Workday App JG";

    public static void main(String[] args) throws Exception {

        LOGGER.info("Application started.");

        CmdConfigOption cmd = Cmd.commandLine(args);

        LoadProperties properties = new LoadProperties();

        String oAuthConsumerKey = properties.getProperty("oauth.consumerKey");
        String oAuthConsumerSecret = properties.getProperty("oauth.consumerSecret");
        String defaultOutputFileName = properties.getProperty("default.output.filename");

        OAuthClient oAuthClient = new OAuthClientImpl(new RestTemplate(), oAuthConsumerKey, oAuthConsumerSecret);

        GitClient gitClient = new GitClientImpl(new RestTemplate());
        List<GitProjectItemTO> gitResult = gitClient.projectGetRequest(cmd.getGitTopicName());

        TwitterClientImpl twitterClient = new TwitterClientImpl(new RestTemplate(), oAuthClient.getOAuth());
        twitterClient.retrieveTweetsForGitProject(gitResult);

        FileUtils.saveResultsToFile(cmd.getFileName(), defaultOutputFileName, gitResult);

        LOGGER.info("Application finished.");
    }

}

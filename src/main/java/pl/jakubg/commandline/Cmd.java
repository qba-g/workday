/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.commandline;

import pl.jakubg.exceptions.WorkdayAppException;
import org.apache.commons.cli.*;

/**
 * Class supports command line parameters parsing.
 */
public class Cmd {

    public static CmdConfigOption commandLine(String[] args) throws WorkdayAppException {

        Options options = new Options();

        options.addOption("t", true, "specify git topic to search");
        options.addOption("f", true, "specify output filename");

        CommandLine cmd;

        try {
            CommandLineParser parser = new DefaultParser();
            cmd = parser.parse(options, args);
        } catch (ParseException pe) {
            throw new WorkdayAppException("Problem when parsing command line params");
        }

        CmdConfigOption cmdConfigOption = new CmdConfigOption();

        if (cmd.hasOption("t")) {
            cmdConfigOption.setGitTopicName(cmd.getOptionValue("t"));
        } else {
            throw new WorkdayAppException("Topic to search not specified");
        }

        if (cmd.hasOption("f")) {
            cmdConfigOption.setFileName(cmd.getOptionValue("f"));
        }

        return cmdConfigOption;
    }
}

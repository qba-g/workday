/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.commandline;

/**
 * Class used to transfer command line selected parameters.
 */
public class CmdConfigOption {

    private String fileName;
    private String gitTopicName;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getGitTopicName() {
        return gitTopicName;
    }

    public void setGitTopicName(String gitTopicName) {
        this.gitTopicName = gitTopicName;
    }
}

/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.oauth;

import pl.jakubg.exceptions.WorkdayAppException;

/**
 * Provide methods to interact with OAuth REST service.
 */
public interface OAuthClient {

    public OAuthTO getOAuth() throws WorkdayAppException;

}

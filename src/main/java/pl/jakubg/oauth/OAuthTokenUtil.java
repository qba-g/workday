/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.oauth;

import org.springframework.util.Base64Utils;
import org.springframework.web.util.UriUtils;

import java.nio.charset.StandardCharsets;

/**
 * Utility class for OAuth related operations.
 */
public class OAuthTokenUtil {

    public static String getEncodedBearerToken(String oAuthConsumerKey, String oAuthConsumerSecret) {

        String oAuthconsumerKeyEnc = UriUtils.encodeQuery(oAuthConsumerKey, StandardCharsets.UTF_8.toString());
        String oAuthConsumerSecretEnc = UriUtils.encodeQuery(oAuthConsumerSecret, StandardCharsets.UTF_8.toString());

        String barerToken = oAuthconsumerKeyEnc + ":" + oAuthConsumerSecretEnc;
        String encodedBarerToken = Base64Utils.encodeToString(barerToken.getBytes());

        return encodedBarerToken;
    }
}

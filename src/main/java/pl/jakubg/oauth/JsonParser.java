/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.oauth;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;

/**
 * Created by kuba on 2019-02-01.
 */
public class JsonParser {

    public static String getOAuthTokenFromJson(ResponseEntity<String> responseEntity) {

        String jsonResponse = responseEntity.getBody().toString();

        JSONObject jsonObject = new JSONObject(jsonResponse);
        String token = jsonObject.getString("access_token");

        return token;
    }


}

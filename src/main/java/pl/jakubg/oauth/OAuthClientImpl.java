/**
 * @author: Jakub Gzyl
 */
package pl.jakubg.oauth;

import pl.jakubg.exceptions.WorkdayAppException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.jakubg.App;

/**
 * Class that perform operations related to Twitter OAuth REST service.
 */
public class OAuthClientImpl implements OAuthClient {

    private static final Logger LOGGER = LogManager.getLogger(OAuthClientImpl.class);

    private static final String OAUTH_TOKEN_URL = "https://api.twitter.com/oauth2/token";

    private RestTemplate restTemplate;

    private String oAuthConsumerKey;
    private String oAuthConsumerSecret;
    private String encodedBearerToken;

    public OAuthClientImpl(RestTemplate restTemplate, String oAuthConsumerKey, String oAuthConsumerSecret) {

        this.restTemplate = restTemplate;

        this.oAuthConsumerKey = oAuthConsumerKey;
        this.oAuthConsumerSecret = oAuthConsumerSecret;
        this.encodedBearerToken = OAuthTokenUtil.getEncodedBearerToken(oAuthConsumerKey, oAuthConsumerSecret);
    }

    private String sendRequestForAccessToken() throws WorkdayAppException {

        LOGGER.info("Preparing request to endpoint: " + OAUTH_TOKEN_URL);

        HttpHeaders httpHeaders = prepareRequestHeaders(encodedBearerToken);
        MultiValueMap<String, String> requestBodyMap = prepareRequestBody();

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(requestBodyMap, httpHeaders);

        ResponseEntity<String> response = restTemplate.postForEntity(OAUTH_TOKEN_URL, requestEntity, String.class);

        if (HttpStatus.OK.equals(response.getStatusCode())) {
            LOGGER.info("Successful request to REST service.");
            LOGGER.info("Body: " + response.getBody());
            return JsonParser.getOAuthTokenFromJson(response);
        } else {
            LOGGER.error("Unsuccessful request to REST service, response: " + response.getStatusCode());
            throw new WorkdayAppException("Can't obtain OAuth token");
        }
    }

    private HttpHeaders prepareRequestHeaders(String encodedBearerToken) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("User-Agent", App.APP_NAME);
        headers.set("Content-Type", "application/x-www-form-urlencoded");
        headers.set("Authorization", "Basic " + encodedBearerToken);

        return headers;
    }

    private MultiValueMap<String, String> prepareRequestBody() {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
        map.add("grant_type", "client_credentials");

        return map;
    }

    public OAuthTO getOAuth() throws WorkdayAppException {

        OAuthTO oAuthTO = new OAuthTO();
        oAuthTO.setoAuthConsumerKey(oAuthConsumerKey);

        String token = sendRequestForAccessToken();
        oAuthTO.setoAuthToken(token);

        return oAuthTO;
    }
}
